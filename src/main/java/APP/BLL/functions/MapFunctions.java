package APP.BLL.functions;

import APP.BLL.models.MeasurementResponseModel;
import APP.DAL.entities.Building;
import APP.DAL.entities.Measurement;
import APP.DAL.entities.Sensor;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.BiConsumer;

import static APP.UTIL.commons.Constants.NO_MEASUREMENTS;
import static APP.UTIL.commons.Constants.NO_SENSORS;

/**
 * Created by Ilya
 * Created on 08.08.2019
 **/

@Component
public class MapFunctions {

    public BiConsumer<HashMap<Integer, Object>, Sensor> getLatestValuesMap() {
        return (map, sensor) -> {
            Optional<Double> optionalLatestValue = latestValue(sensor);
            map.put(sensor.getOid(), optionalLatestValue.isPresent() ? optionalLatestValue.get() : NO_MEASUREMENTS);
        };
    }

    public BiConsumer<HashMap<Integer, Object>, Building> getAverageValuesMap() {
        return (map, building) -> map.put(building.getOid(), averageValue(building));
    }

    public MeasurementResponseModel toResponsePair(Measurement m) {
        return new MeasurementResponseModel(m.getTime(), m.getValue());
    }

    private Optional<Double> latestValue(Sensor s) {
        Measurement latestMeasurement = s.getLatestMeasurement();
        if (null != latestMeasurement) return Optional.ofNullable(latestMeasurement.getValue());
        // fallback for initial dataset when there is no latest measurement:
        // get all existing measurements of this sensor and find latest by time
        return s.getMeasurements().stream()
                .max(Comparator.comparingLong(Measurement::getTime))
                .map(Measurement::getValue);

    }

    private Object averageValue(Building b) {
        List<Sensor> sensors = (List<Sensor>) b.getSensors();
        List<Double> latestValues = new ArrayList();
        sensors.forEach(s -> latestValue(s).ifPresent(latestValues::add));
        OptionalDouble average = latestValues.stream().mapToDouble(v -> v).average();
        // no data msg if building has no sensors attached or sensors have no measurements
        return average.isPresent() ? average.getAsDouble() : sensors.isEmpty() ? NO_SENSORS : NO_MEASUREMENTS;
    }
}
