package APP.BLL.services;

import APP.BLL.models.AverageValuesResponseModel;
import APP.BLL.models.LatestValuesResponseModel;
import APP.BLL.models.SaveMeasurementModel;
import APP.BLL.models.SensorResponseModel;
import APP.DAL.entities.Measurement;
import APP.UTIL.exceptions.GeneralErrorException;
import APP.UTIL.exceptions.NoObjectFoundException;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/


public interface GeneralService {
    Measurement saveMeasurement(SaveMeasurementModel model) throws Exception;

    SensorResponseModel getMeasurementsBySensorAndPeriod(Integer sensorOid, Long start, Long end) throws Exception;

    LatestValuesResponseModel getLatestValuesByBuilding(Integer buildingOid) throws NoObjectFoundException;

    AverageValuesResponseModel getAverageValues();

    void updateLatestMeasurement(Measurement measurement) throws GeneralErrorException;
}
