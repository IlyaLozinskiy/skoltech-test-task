package APP.BLL.services;

import APP.BLL.functions.MapFunctions;
import APP.BLL.models.*;
import APP.DAL.entities.Building;
import APP.DAL.entities.Measurement;
import APP.DAL.entities.Sensor;
import APP.DAL.repositories.BuildingRepo;
import APP.DAL.repositories.MeasurementRepo;
import APP.DAL.repositories.SensorRepo;
import APP.UTIL.exceptions.GeneralErrorException;
import APP.UTIL.logs.LogUtils;
import APP.UTIL.exceptions.BadDataException;
import APP.UTIL.exceptions.NoObjectFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static APP.UTIL.commons.Constants.*;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

@Service
public class GeneralServiceImpl implements GeneralService {
    public static final Logger logger = Logger.getLogger("GeneralServiceImpl");

    @Autowired
    private BuildingRepo buildingRepo;

    @Autowired
    private SensorRepo sensorRepo;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Autowired
    private MapFunctions mapFunctions;

    @Override
    public Measurement saveMeasurement(SaveMeasurementModel model) throws Exception {
        Building building = buildingRepo.findByOid(model.buildingOid);
        if (null == building) throw new NoObjectFoundException(BUILDING_NOT_FOUND);

        Sensor sensor = sensorRepo.findByOid(model.sensorOid);
        if (null == sensor) throw new NoObjectFoundException(SENSOR_NOT_FOUND);

        if (!sensor.getBuilding().getOid().equals(building.getOid()))
            throw new NoObjectFoundException(SENSOR_BELONGS_TO_ANOTHER_BUILDING);

        if (model.time < ZERO)
            throw new BadDataException(NEGATIVE_TIME);

        try {
            Measurement measurement = new Measurement(sensor, model.time, model.value);
            measurementRepo.save(measurement);
            updateLatestMeasurement(measurement);
            return measurement;
        } catch (Exception e) {
            LogUtils.error(logger, "saveMeasurement", ERROR_OCCURRED, e, new Object[]{model});
            throw new Exception(e);
        }
    }

    @Override
    public SensorResponseModel getMeasurementsBySensorAndPeriod(Integer sensorOid, Long start, Long end) throws Exception {
        Sensor sensor = sensorRepo.findByOid(sensorOid);
        if (null == sensor) throw new NoObjectFoundException(SENSOR_NOT_FOUND);

        List<Measurement> result;
        if (null == start && null == end) result = measurementRepo.findBySensorOid(sensorOid);
        else if (null == start) result = measurementRepo.findBySensorOidAndTimeLessThanEqual(sensorOid, end);
        else if (null == end) result = measurementRepo.findBySensorOidAndTimeGreaterThanEqual(sensorOid, start);
        else if (start >= end) throw new BadDataException(WRONG_PERIOD);
        else result = measurementRepo.findBySensorOidAndTimeGreaterThanEqualAndTimeLessThanEqual(sensorOid, start, end);

        try {
            return new SensorResponseModel() {{
                sensorOid = sensor.getOid();
                buildingOid = sensor.getBuilding().getOid();
                count = result.size();
                oidToMeasurement = (HashMap<Integer, MeasurementResponseModel>) result.stream()
                        .collect(Collectors.toMap(Measurement::getOid, mapFunctions::toResponsePair));
            }};
        } catch (Exception e) {
            LogUtils.error(logger, "getMeasurementsBySensorAndPeriod", ERROR_OCCURRED, e, new Object[]{sensorOid, start, end});
            throw new Exception(e);
        }
    }

    @Override
    @Transactional
    public LatestValuesResponseModel getLatestValuesByBuilding(Integer oid) throws NoObjectFoundException {
        Building building = buildingRepo.findByOid(oid);
        if (null == building) throw new NoObjectFoundException(BUILDING_NOT_FOUND);

        return new LatestValuesResponseModel() {{
            buildingOid = oid;
            sensorOidToValue = building.getSensors().stream()
                    .collect(HashMap::new, mapFunctions.getLatestValuesMap(), HashMap::putAll);
        }};
    }

    @Override
    @Transactional
    public AverageValuesResponseModel getAverageValues() {
        return new AverageValuesResponseModel() {{
            buildingOidToValue = StreamSupport.stream(buildingRepo.findAll().spliterator(), false)
                    .collect(HashMap::new, mapFunctions.getAverageValuesMap(), HashMap::putAll);
        }};
    }

    @Override
    @Transactional
    public void updateLatestMeasurement(Measurement measurement) throws GeneralErrorException {
        Sensor sensor = measurement.getSensor();
        if (null == sensor.getLatestMeasurement()) sensor.setLatestMeasurement(measurement);
        else {
            Measurement latest = measurementRepo.findByOid(sensor.getLatestMeasurement().getOid());
            if (null == latest || latest.getTime() < measurement.getTime())
                sensor.setLatestMeasurement(measurement);
        }

        try {
            sensorRepo.save(sensor);
        } catch (Exception e) {
            LogUtils.error(logger, "updateLatestMeasurement", ERROR_OCCURRED, e, new Object[]{measurement});
            throw new GeneralErrorException(UPDATE_ERROR);
        }
    }
}
