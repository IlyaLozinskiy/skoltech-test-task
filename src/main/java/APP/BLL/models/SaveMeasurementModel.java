package APP.BLL.models;

import APP.UTIL.commons.BaseObject;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/


public class SaveMeasurementModel extends BaseObject {
    private static final long serialVersionUID = 7842614052065190508L;
    @JsonProperty("b")
    public int buildingOid;
    @JsonProperty("s")
    public int sensorOid;
    @JsonProperty("t")
    public long time;
    @JsonProperty("v")
    public double value;

    public SaveMeasurementModel() {
    }

    public int getBuildingOid() {
        return buildingOid;
    }

    @JsonProperty("b")
    public void setBuildingOid(int buildingOid) {
        this.buildingOid = buildingOid;
    }

    public int getSensorOid() {
        return sensorOid;
    }

    @JsonProperty("s")
    public void setSensorOid(int sensorOid) {
        this.sensorOid = sensorOid;
    }

    public long getTime() {
        return time;
    }

    @JsonProperty("t")
    public void setTime(long time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    @JsonProperty("v")
    public void setValue(double value) {
        this.value = value;
    }
}

