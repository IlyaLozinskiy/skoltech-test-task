package APP.BLL.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/


public class SensorResponseModel extends ResponseModel {
    private static final long serialVersionUID = -777053502938235634L;
    @JsonProperty("b")
    public int buildingOid;
    @JsonProperty("s")
    public int sensorOid;
    @JsonProperty("c")
    public int count;
    @JsonProperty("m")
    public HashMap<Integer, MeasurementResponseModel> oidToMeasurement;


}
