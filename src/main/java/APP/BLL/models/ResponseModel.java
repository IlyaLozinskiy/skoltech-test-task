package APP.BLL.models;

import APP.UTIL.commons.BaseObject;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


public class ResponseModel extends BaseObject {
    private static final long serialVersionUID = -2401544648512144339L;
    private final long serverTime = System.currentTimeMillis();
    private boolean error;

    void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public long getServerTime() {
        return serverTime;
    }
}
