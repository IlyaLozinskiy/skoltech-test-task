package APP.BLL.models;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


public class ErrorResponseModel extends ResponseModel {
    private static final long serialVersionUID = -4929677024542437824L;
    private final String url;
    private final String msg;
    private final int code;

    public ErrorResponseModel(String url, String msg, int code) {
        this.msg = msg;
        setError(true);
        this.url = url;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public String getUrl() {
        return url;
    }

    public int getCode() {
        return code;
    }
}