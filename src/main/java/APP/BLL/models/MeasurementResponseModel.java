package APP.BLL.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Ilya
 * Created on 08.08.2019
 **/
public class MeasurementResponseModel implements Serializable {
    private static final long serialVersionUID = -4673209477619008968L;
    @JsonProperty("t")
    Long time;
    @JsonProperty("v")
    Double value;

    public MeasurementResponseModel(Long time, Double value) {
        this.time = time;
        this.value = value;
    }
}
