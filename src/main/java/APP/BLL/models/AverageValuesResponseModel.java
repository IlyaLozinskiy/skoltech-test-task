package APP.BLL.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

public class AverageValuesResponseModel extends ResponseModel {
    private static final long serialVersionUID = 4388732327808673255L;
    @JsonProperty("v")
    public HashMap<Integer, Object> buildingOidToValue;
}
