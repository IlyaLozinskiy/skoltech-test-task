package APP.BLL.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

public class LatestValuesResponseModel extends ResponseModel {
    private static final long serialVersionUID = 4226869440748726128L;
    @JsonProperty("b")
    public int buildingOid;
    @JsonProperty("v")
    public HashMap<Integer, Object> sensorOidToValue;
}
