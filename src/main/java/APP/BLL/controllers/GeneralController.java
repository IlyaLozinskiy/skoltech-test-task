package APP.BLL.controllers;

import APP.BLL.models.*;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.entities.Measurement;
import APP.UTIL.exceptions.GeneralErrorException;
import APP.UTIL.exceptions.NoDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static APP.UTIL.commons.Constants.*;

@RestController
@RequestMapping("/api")
public class GeneralController {

    @Autowired
    GeneralServiceImpl service;

    @GetMapping(path = "/status")
    public @ResponseBody
    ResponseModel status() {
        return new ResponseModel();
    }

    @PostMapping(path = "/save")
    public @ResponseBody
    ResponseModel save(@RequestBody SaveMeasurementModel m) throws Exception {
        Measurement measurement = service.saveMeasurement(m);

        if (null == measurement.getOid())
            throw new GeneralErrorException(SAVE_ERROR);

        return new ResponseModel();
    }

    @GetMapping(path = "/sensor")
    public @ResponseBody
    ResponseModel getMeasurementsBySensorAndPeriod(@RequestParam Integer oid, @RequestParam(required = false) Long start, @RequestParam(required = false) Long end) throws Exception {
        if (null == oid) throw new NoDataException(SPECIFY_SENSOR);
        return service.getMeasurementsBySensorAndPeriod(oid, start, end);
    }

    @GetMapping(path = "/building-current")
    public @ResponseBody
    ResponseModel getLatestValuesByBuilding(@RequestParam Integer oid) throws Exception {
        if (null == oid) throw new NoDataException(SPECIFY_BUILDING);
        return service.getLatestValuesByBuilding(oid);
    }

    @GetMapping(path = "/average-current")
    public @ResponseBody
    ResponseModel getAverageValues() {
        return service.getAverageValues();
    }
}
