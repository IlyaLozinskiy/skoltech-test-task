package APP.BLL.controllers;

import APP.BLL.models.ErrorResponseModel;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;

import static APP.UTIL.commons.Constants.*;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


@RestController
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler implements ErrorController {
    private static final Logger logger = Logger.getLogger("ExceptionController");

    @RequestMapping("/error")
    public @ResponseBody
    ErrorResponseModel handleBadUrlError(HttpServletRequest req, Exception e) {
        int code;
        String msg;
        String url;
        try {
            code = getErrorCode(e);
            url = (String) req.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI);
            msg = e.getMessage() == null ? BAD_URL : e.getMessage();
        } catch (Exception ex) {
            url = UNKNOWN_URL;
            code = UNKNOWN_ERROR_CODE;
            msg = ex.getMessage();
        }
        return new ErrorResponseModel(url, msg, code);

    }

    @ExceptionHandler({Throwable.class})
    public ResponseEntity handleError(Exception ex, WebRequest request) {
        String uri = ((ServletWebRequest) request).getRequest().getRequestURI();
        ErrorResponseModel response = new ErrorResponseModel(uri, ex.getMessage(), getErrorCode(ex));
        return new ResponseEntity(response, new HttpHeaders(), HttpStatus.CONFLICT);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

    private static int getErrorCode(Exception e) {
        // TODO: 07.08.2019 map error codes;
        return UNKNOWN_ERROR_CODE;
    }
}


