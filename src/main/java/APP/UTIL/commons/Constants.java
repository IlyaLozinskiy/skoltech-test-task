package APP.UTIL.commons;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


public class Constants {
    public static final String BUILDING_NOT_FOUND = "Building not found";
    public static final String SENSOR_NOT_FOUND = "Sensor not found";
    public static final String SPECIFY_SENSOR = "Specify sensor id";
    public static final String SPECIFY_BUILDING = "Specify building id";
    public static final String WRONG_PERIOD = "Wrong time period";
    public static final String SAVE_ERROR = "Error occurred while saving measurement";
    public static final String UPDATE_ERROR = "Error occurred while updating latest measurement";
    public static final String NEGATIVE_TIME = "Time must be positive number of milliseconds";
    public static final String SENSOR_BELONGS_TO_ANOTHER_BUILDING = "Sensor belongs to another building";
    public static final String NO_SENSORS = "No sensors";
    public static final String NO_MEASUREMENTS = "No measurements";
    public static final String UNKNOWN_URL = "Unknown URL";
    public static final String ERROR_PATH = "/error";
    public static final String BAD_URL = "Bad URL";
    public static final String ERROR_OCCURRED = "Error occurred";
    public static final int UNKNOWN_ERROR_CODE = -1;
    public static final long SECOND = 1000L;
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
    public static final int ZERO = 0;
}
