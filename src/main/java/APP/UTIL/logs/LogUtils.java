package APP.UTIL.logs;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtils {

    public void error(Logger logger, String methodName, String message) {
        logger.log(Level.SEVERE, String.format("[%s] %s", methodName, message));
    }

    public void error(Logger logger, String message, Throwable error) {
        logger.log(Level.SEVERE, message, error);
    }

    public void error(Logger logger, String methodName, String message, Throwable error) {
        logger.log(Level.SEVERE, String.format("[%s] %s", methodName, message), error);
    }

    public static void error(Logger logger, String methodName, String message, Throwable error, Object[] calledWithArgs) {
        StringBuilder args = new StringBuilder();
        for (int i = 0; i < calledWithArgs.length; i++) {
            args.append("\"").append(i).append("\": ").append(calledWithArgs[i]).append(", ");
        }
        logger.log(Level.SEVERE, String.format("[%s called with args: %s] %s", methodName, args.toString(), message), error);
    }
}
