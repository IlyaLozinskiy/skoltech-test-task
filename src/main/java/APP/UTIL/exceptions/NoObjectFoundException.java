package APP.UTIL.exceptions;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


public class NoObjectFoundException extends Exception {

    public NoObjectFoundException() {
    }

    public NoObjectFoundException(String message) {
        super(message);
    }

    public NoObjectFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoObjectFoundException(Throwable cause) {
        super(cause);
    }

    public NoObjectFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
