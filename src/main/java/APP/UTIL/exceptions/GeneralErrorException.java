package APP.UTIL.exceptions;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


public class GeneralErrorException extends Exception {
    public GeneralErrorException() {
    }

    public GeneralErrorException(String message) {
        super(message);
    }

    public GeneralErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralErrorException(Throwable cause) {
        super(cause);
    }

    public GeneralErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
