package APP.DAL.repositories;

import APP.DAL.entities.Measurement;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

public interface MeasurementRepo extends CrudRepository<Measurement, Integer> {

    Measurement findByOid(int measurementOid);

    Measurement findFirstByOrderByOidDesc();

    List<Measurement> findBySensorOid(Integer sensorOid);

    List<Measurement> findBySensorOidAndTimeLessThanEqual(Integer sensorOid, Long end);

    List<Measurement> findBySensorOidAndTimeGreaterThanEqual(Integer sensorOid, Long start);

    List<Measurement> findBySensorOidAndTimeGreaterThanEqualAndTimeLessThanEqual(Integer sensorOid, Long start, Long end);
}
