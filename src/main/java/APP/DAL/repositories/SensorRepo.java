package APP.DAL.repositories;

import APP.DAL.entities.Sensor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

public interface SensorRepo extends CrudRepository<Sensor, Integer> {

    Sensor findByOid(int sensorOid);

    List<Sensor> findByBuildingOid(Integer buildingOid);
}