package APP.DAL.repositories;

import APP.DAL.entities.Building;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

public interface BuildingRepo extends CrudRepository<Building, Integer> {
    Building findByOid(int buildingOid);
}
