package APP.DAL.entities;

import APP.UTIL.commons.BaseObject;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

@Entity
public class Measurement extends BaseObject {
    private static final long serialVersionUID = -1334442291270051396L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer oid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sensorOid", updatable = false)
    @Fetch(FetchMode.JOIN)
    private Sensor sensor;

    private Long time;

    private Double value;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Measurement() {
    }

    public Measurement(Sensor sensor, Long time, Double value) {
        this.sensor = sensor;
        this.time = time;
        this.value = value;
    }
}
