package APP.DAL.entities;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

import APP.UTIL.commons.BaseObject;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

@Entity
public class Building extends BaseObject {

    private static final long serialVersionUID = 6321300487879150269L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer oid;

    @OneToMany(mappedBy = "building")
    private Collection<Sensor> sensors;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Collection<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(Collection<Sensor> sensors) {
        this.sensors = sensors;
    }

    public Building() {
        sensors = Collections.EMPTY_LIST;
    }
}