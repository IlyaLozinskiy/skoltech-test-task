package APP.DAL.entities;

import APP.UTIL.commons.BaseObject;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Ilya
 * Created on 06.08.2019
 **/

@Entity
public class Sensor extends BaseObject {
    private static final long serialVersionUID = 1443548450046225932L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer oid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buildingOid", updatable = false)
    @Fetch(FetchMode.JOIN)
    private Building building;

    @OneToMany(mappedBy = "sensor")
    private Collection<Measurement> measurements;

    @OneToOne(targetEntity = Measurement.class, fetch = FetchType.EAGER)
    private Measurement latestMeasurement;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Collection<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(Collection<Measurement> measurements) {
        this.measurements = measurements;
    }

    public Measurement getLatestMeasurement() {
        return latestMeasurement;
    }

    public void setLatestMeasurement(Measurement latestMeasurement) {
        this.latestMeasurement = latestMeasurement;
    }

    public Sensor() {
        measurements = Collections.EMPTY_LIST;
    }

    public Sensor(Building building) {
        this();
        this.building = building;
    }
}

