package APP;

import APP.DAL.entities.Building;
import APP.DAL.entities.Measurement;
import APP.DAL.entities.Sensor;
import APP.DAL.repositories.BuildingRepo;
import APP.DAL.repositories.MeasurementRepo;
import APP.DAL.repositories.SensorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.web.servlet.DispatcherServlet;

import java.util.Calendar;
import java.util.Random;
import java.util.logging.Logger;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
    public static final Logger logger = Logger.getGlobal();

    @Autowired
    private SensorRepo sensorRepo;

    @Autowired
    private BuildingRepo buildingRepo;

    @Autowired
    private MeasurementRepo measurementRepo;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        DispatcherServlet dispatcherServlet = (DispatcherServlet) ctx.getBean("dispatcherServlet");
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
        // TODO: 07.08.2019 manage with timezones - client can send his timezone
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onReady() {
        logger.info("App is ready");

//       Set of data for testing, first part is in import.sql
//       Uncomment to generate set of 1250 db entries
//
//        long now = Calendar.getInstance().getTimeInMillis();
//        for (int i = 0; i < 5; i++) {
//            Building building = new Building();
//            buildingRepo.save(building);
//            for (int j = 0; j < 10; j++) {
//                Sensor sensor = new Sensor();
//                sensor.setBuilding(building);
//                sensorRepo.save(sensor);
//                for (int k = 0; k < 25; k++) {
//                    int seed = k % 2 == 0 ? i * j * k : -i * j * k * new Random().nextInt(10);
//                    long rndTime = now + seed;
//                    double rndValue = new Random(seed).nextDouble() + seed * 0.001;
//                    Measurement measurement = new Measurement();
//                    measurement.setTime(rndTime);
//                    measurement.setValue(rndValue);
//                    measurement.setSensor(sensor);
//                    measurementRepo.save(measurement);
//                }
//            }
//        }
    }
}
