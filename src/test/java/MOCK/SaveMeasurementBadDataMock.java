package MOCK;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Ilya
 * Created on 08.08.2019
 **/


public class SaveMeasurementBadDataMock implements Serializable {
    private static final long serialVersionUID = 7842614052065190508L;
    @JsonProperty("b")
    public Object buildingOid;
    @JsonProperty("s")
    public Object sensorOid;
    @JsonProperty("t")
    public Object time;
    @JsonProperty("v")
    public Object value;

    public SaveMeasurementBadDataMock() {
    }

    public Object getBuildingOid() {
        return buildingOid;
    }

    @JsonProperty("b")
    public void setBuildingOid(int buildingOid) {
        this.buildingOid = buildingOid;
    }

    public Object getSensorOid() {
        return sensorOid;
    }

    @JsonProperty("s")
    public void setSensorOid(int sensorOid) {
        this.sensorOid = sensorOid;
    }

    public Object getTime() {
        return time;
    }

    @JsonProperty("t")
    public void setTime(long time) {
        this.time = time;
    }

    public Object getValue() {
        return value;
    }

    @JsonProperty("v")
    public void setValue(double value) {
        this.value = value;
    }

}
