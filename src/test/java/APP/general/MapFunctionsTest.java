package APP.general;

import APP.BLL.functions.MapFunctions;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.entities.Building;
import APP.DAL.entities.Measurement;
import APP.DAL.entities.Sensor;
import APP.DAL.repositories.BuildingRepo;
import APP.DAL.repositories.MeasurementRepo;
import APP.DAL.repositories.SensorRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import static APP.UTIL.commons.Constants.NO_MEASUREMENTS;
import static APP.UTIL.commons.Constants.NO_SENSORS;
import static org.junit.Assert.*;

/**
 * Created by Ilya
 * Created on 09.08.2019
 **/


@RunWith(SpringRunner.class)
@SpringBootTest()
public class MapFunctionsTest extends BaseTest {

    @Autowired
    private MapFunctions functions;

    @Autowired
    private GeneralServiceImpl generalService;

    @Autowired
    private BuildingRepo buildingRepo;

    @Autowired
    private SensorRepo sensorRepo;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Test
    public void latestValue() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Sensor sensor = sensorRepo.findByOid(1);
        Method latestValue = getPrivateMethod("latestValue", MapFunctions.class);
        Optional<Double> result = (Optional<Double>) latestValue.invoke(this.functions, sensor);
        assertTrue(result.isPresent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void latestValueSensorIsNull() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method latestValue = getPrivateMethod("latestValue", MapFunctions.class);
        latestValue.invoke(this.functions, null);
    }

    @Test
    public void latestValueSensorHasNoMeasurements() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Sensor sensor = new Sensor(buildingRepo.findByOid(1));
        sensorRepo.save(sensor);
        Method latestValue = getPrivateMethod("latestValue", MapFunctions.class);
        Optional<Double> result = (Optional<Double>) latestValue.invoke(this.functions, sensor);
        assertFalse(result.isPresent());
    }

    @Test
    public void latestValueSensorIsNotInDB() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Sensor sensor = new Sensor(new Building());
        Method latestValue = getPrivateMethod("latestValue", MapFunctions.class);
        Optional<Double> result = (Optional<Double>) latestValue.invoke(this.functions, sensor);
        assertFalse(result.isPresent());
    }

    @Test
    public void latestValueSensorIsNotInDBAndHasNoBuilding() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Sensor sensor = new Sensor();
        Method latestValue = getPrivateMethod("latestValue", MapFunctions.class);
        Optional<Double> result = (Optional<Double>) latestValue.invoke(this.functions, sensor);
        assertFalse(result.isPresent());
    }

    @Test
    @Transactional
    public void averageValue() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Building building = buildingRepo.findByOid(1);
        Method averageValue = getPrivateMethod("averageValue", MapFunctions.class);
        Object result = averageValue.invoke(this.functions, building);
        assertNotNull(result);
    }

    @Test
    public void averageValueBuildingHasNoSensors() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Building building = new Building();
        Method latestValue = getPrivateMethod("averageValue", MapFunctions.class);
        Object result = latestValue.invoke(this.functions, building);
        assertEquals(NO_SENSORS, result);
    }
}