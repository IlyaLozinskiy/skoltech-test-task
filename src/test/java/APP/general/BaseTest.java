package APP.general;

import APP.BLL.models.SaveMeasurementModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Method;
import java.util.Random;
import java.util.logging.Logger;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/


public class BaseTest {
    protected static final Logger logger = Logger.getLogger("Testing Logger");

    public long now() {
        return System.currentTimeMillis();
    }

    public long rndL() {
        long leftLimit = 1L;
        long rightLimit = 100L;
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    public long rndL(long leftLimit, long rightLimit) {
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    public int rndI() {
        int leftLimit = 1;
        int rightLimit = 100;
        return leftLimit + (int) (new Random(System.currentTimeMillis()).nextFloat() * (rightLimit - leftLimit));
    }

    public int rndI(int leftLimit, int rightLimit) {
        return leftLimit + (int) (new Random(System.currentTimeMillis()).nextFloat() * (rightLimit - leftLimit));
    }

    public double rndD() {
        double leftLimit = -100D;
        double rightLimit = 100D;
        return leftLimit + new Random(System.currentTimeMillis()).nextDouble() * (rightLimit - leftLimit);
    }

    public double rndD(double leftLimit, double rightLimit) {
        return leftLimit + new Random(System.currentTimeMillis()).nextDouble() * (rightLimit - leftLimit);
    }

    protected SaveMeasurementModel getModel(Integer _buildingOid, Integer _sensorOid, Long _time, Double _value) {
        // TODO: 09.08.2019 refactor all such usages
        return new SaveMeasurementModel() {{
            sensorOid = _sensorOid;
            buildingOid = _buildingOid;
            time = _time;
            value = _value;
        }};
    }

    void saveMeasurementRequest(SaveMeasurementModel measurement, MockMvc mockMvc) throws Exception {
        String body = (new ObjectMapper()).valueToTree(measurement).toString();
        mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false));
    }

    Method getPrivateMethod(String methodName, Class klass) throws NoSuchMethodException {
        Method[] declaredMethods = klass.getDeclaredMethods();
        Method target = null;
        for (int i = 0; i < declaredMethods.length; i++) {
            if (declaredMethods[i].getName().equals(methodName)) {
                target = declaredMethods[i];
                break;
            }
        }
        if (null == target) throw new NoSuchMethodException();
        target.setAccessible(true);
        return target;
    }
}
