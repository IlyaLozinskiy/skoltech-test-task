package APP.general;


import APP.BLL.controllers.ExceptionController;
import APP.BLL.controllers.GeneralController;
import APP.BLL.models.SaveMeasurementModel;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.entities.Building;
import APP.DAL.entities.Sensor;
import APP.DAL.repositories.BuildingRepo;
import APP.DAL.repositories.MeasurementRepo;
import APP.DAL.repositories.SensorRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static APP.UTIL.commons.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ApplicationTest extends BaseTest {

    @Autowired
    private GeneralController generalController;

    @Autowired
    private ExceptionController exceptionController;

    @Autowired
    private GeneralServiceImpl generalService;

    @Autowired
    private SensorRepo sensorRepo;

    @Autowired
    private BuildingRepo buildingRepo;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Test
    public void contextLoads() {
        assertThat(generalController).isNotNull();
        assertThat(exceptionController).isNotNull();
        assertThat(generalService).isNotNull();
        assertThat(sensorRepo).isNotNull();
        assertThat(buildingRepo).isNotNull();
        assertThat(measurementRepo).isNotNull();
    }

    @Test
    public void serverStatus() throws Exception {
        this.mockMvc.perform(get("/api/status")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false));
    }

    @Test
    public void indexReturnsError() {
        String response = this.restTemplate.getForObject("http://localhost:" + port + "/", String.class);
        assertThat(response).contains("\"error\":true").contains(BAD_URL);
    }

    @Test
    public void wholeInteractionCycleThroughRequests() throws Exception {
        // create new building and sensors through code
        Building newBuilding = buildingRepo.save(new Building());
        Sensor newSensor_1 = new Sensor();
        newSensor_1.setBuilding(newBuilding);
        Sensor newSensor_2 = new Sensor();
        newSensor_2.setBuilding(newBuilding);
        Sensor newSensor_3 = new Sensor();
        newSensor_3.setBuilding(newBuilding);
        Sensor newSensor_4 = new Sensor();
        newSensor_4.setBuilding(newBuilding);
        sensorRepo.save(newSensor_1);
        sensorRepo.save(newSensor_2);
        sensorRepo.save(newSensor_3);
        sensorRepo.save(newSensor_4);

        // save new measurements through API request
        SaveMeasurementModel new_measurement_1 = getModel(newBuilding.getOid(), newSensor_1.getOid(), now() + SECOND, 1d);
        SaveMeasurementModel new_measurement_2 = getModel(newBuilding.getOid(), newSensor_2.getOid(), now(), 2d);
        SaveMeasurementModel new_measurement_3 = getModel(newBuilding.getOid(), newSensor_3.getOid(), now() + SECOND, 3d); // <-- latest by time (sensor_3)
        SaveMeasurementModel new_measurement_4 = getModel(newBuilding.getOid(), newSensor_3.getOid(), now(), 4d); // <-- latest by how it came to the server (sensor_3)
        saveMeasurementRequest(new_measurement_1, mockMvc);
        saveMeasurementRequest(new_measurement_2, mockMvc);
        saveMeasurementRequest(new_measurement_3, mockMvc);
        saveMeasurementRequest(new_measurement_4, mockMvc);

        // get latest value per sensor for building
        this.mockMvc.perform(get("/api/building-current").param("oid", newBuilding.getOid().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.v." + newSensor_1.getOid().toString()).value(1d))
                .andExpect(jsonPath("$.v." + newSensor_2.getOid().toString()).value(2d))
                .andExpect(jsonPath("$.v." + newSensor_3.getOid().toString()).value(3d))
                .andExpect(jsonPath("$.v." + newSensor_4.getOid().toString()).value(NO_MEASUREMENTS));


        // get average values per building
        this.mockMvc.perform(get("/api/average-current")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.v." + newBuilding.getOid().toString()).value(2d));
    }
}
