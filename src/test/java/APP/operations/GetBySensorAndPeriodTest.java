package APP.operations;

import APP.BLL.models.SaveMeasurementModel;
import APP.BLL.models.SensorResponseModel;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.repositories.MeasurementRepo;
import APP.UTIL.exceptions.BadDataException;
import APP.UTIL.exceptions.NoObjectFoundException;
import APP.general.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.stream.StreamSupport;

import static APP.UTIL.commons.Constants.DAY;
import static APP.UTIL.commons.Constants.SENSOR_NOT_FOUND;
import static org.apache.catalina.util.ConcurrentDateFormat.GMT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetBySensorAndPeriodTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GeneralServiceImpl generalService;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void getBySensorAndPeriod() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Integer newOid = generalService.saveMeasurement(measurement).getOid();
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(measurement.sensorOid, measurement.time - 1000L, measurement.time + 1000L);
        assertTrue(setFromDB.oidToMeasurement.containsKey(newOid));
    }

    @Test
    public void getAllExistingBySensorAndPeriod() throws Exception {
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 1);
        long actualCount = measurementRepo.findBySensorOid(1).size();
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(1, 0L, end.getTimeInMillis());
        assertTrue(setFromDB.oidToMeasurement.size() > 0);
        assertEquals(setFromDB.oidToMeasurement.size(), actualCount);
    }

    @Test(expected = BadDataException.class)
    public void getBySensorAndPeriodStartGreaterThanEnd() throws Exception {
        Calendar start = Calendar.getInstance();
        start.add(Calendar.YEAR, 1);
        Calendar end = Calendar.getInstance();
        end.add(Calendar.DATE, -1);
        generalService.getMeasurementsBySensorAndPeriod(1, start.getTimeInMillis(), end.getTimeInMillis());
    }

    @Test
    public void getBySensorAndPeriodNoStartTime() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Integer newOid = generalService.saveMeasurement(measurement).getOid();
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(measurement.sensorOid, null, measurement.time + 1000L);
        assertTrue(setFromDB.oidToMeasurement.containsKey(newOid));
    }

    @Test
    public void getBySensorAndPeriodNoEndTime() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Integer newOid = generalService.saveMeasurement(measurement).getOid();
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(measurement.sensorOid, measurement.time - 1000L, null);
        assertTrue(setFromDB.oidToMeasurement.containsKey(newOid));
    }

    @Test
    public void getBySensorAndPeriodNoTime() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Integer newOid = generalService.saveMeasurement(measurement).getOid();
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(measurement.sensorOid, null, null);
        assertTrue(setFromDB.oidToMeasurement.containsKey(newOid));
    }

    @Test
    public void getBySensorAndPeriodOutOfRange() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(measurement.sensorOid, measurement.time + 1000L, measurement.time + 2000L);
        assertFalse(setFromDB.oidToMeasurement.containsKey(measurement.time));
    }

    @Test(expected = NoObjectFoundException.class)
    public void getBySensorAndPeriodNoSensor() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = -1;
            buildingOid = 10;
            time = now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
        SensorResponseModel setFromDB = generalService.getMeasurementsBySensorAndPeriod(measurement.sensorOid, measurement.time + 1000L, measurement.time + 2000L);
        assertFalse(setFromDB.oidToMeasurement.containsKey(measurement.time));
    }

    @Test
    public void getBySensorAndPeriodIsNotEmpty() throws Exception {
        long start = now() - DAY * 30;
        long end = now() + DAY * 30;
        int actualSize = generalService.getMeasurementsBySensorAndPeriod(1, start, end).oidToMeasurement.size();
        assertTrue(actualSize > 0);
    }

    @Test
    public void getBySensorAndPeriodThroughRequest() throws Exception {
        long start = now() - DAY * 30;
        long end = now() + DAY * 30;
        int actualSize = generalService.getMeasurementsBySensorAndPeriod(1, start, end).oidToMeasurement.size();

        this.mockMvc.perform(get("/api/sensor")
                .param("oid", "1")
                .param("start", Long.toString(start))
                .param("end", Long.toString(end)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.s").value(1))
                .andExpect(jsonPath("$.c").value(actualSize));
    }

    @Test
    public void getBySensorAndPeriodThroughRequestForYear2017() throws Exception {
        Calendar _2017 = Calendar.getInstance();
        _2017.set(2017, Calendar.JANUARY, 1, 0, 0, 0);
        _2017.set(Calendar.MILLISECOND, 0);
        _2017.setTimeZone(GMT);
        Calendar _2018 = Calendar.getInstance();
        _2018.set(2018, Calendar.JANUARY, 1, 0, 0, 0);
        _2018.set(Calendar.MILLISECOND, 0);
        _2018.setTimeZone(GMT);

        this.mockMvc.perform(get("/api/sensor")
                .param("oid", "3")
                .param("start", Long.toString(_2017.getTimeInMillis()))
                .param("end", Long.toString(_2018.getTimeInMillis())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.s").value(3))
                .andExpect(jsonPath("$.c").value(2));
    }

    @Test
    public void getBySensorAndPeriodStartGreaterThanEndTroughRequest() throws Exception {
        long start = now() - DAY * 30;
        long end = now() + DAY * 30;
        this.mockMvc.perform(get("/api/sensor")
                .param("oid", "1")
                .param("start", Long.toString(end))
                .param("end", Long.toString(start)))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.error").value(true));
    }

    @Test
    public void invalidSensorCallReturnsError() {
        String response = this.restTemplate.getForObject("http://localhost:" + port + "/api/sensor?oid=-1", String.class);
        assertThat(response).contains("\"error\":true").contains(SENSOR_NOT_FOUND);
    }
}

