package APP.operations;

import APP.BLL.models.SaveMeasurementModel;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.repositories.SensorRepo;
import APP.UTIL.exceptions.NoDataException;
import MOCK.SaveMeasurementBadDataMock;
import APP.UTIL.exceptions.BadDataException;
import APP.general.BaseTest;
import APP.DAL.entities.Measurement;
import APP.DAL.repositories.MeasurementRepo;
import APP.UTIL.exceptions.NoObjectFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;

import static APP.UTIL.commons.Constants.DAY;
import static APP.UTIL.commons.Constants.SECOND;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Ilya
 * Created on 07.08.2019
 **/

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest()
public class SaveMeasurementTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GeneralServiceImpl generalService;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Autowired
    private SensorRepo sensorRepo;

    @Test
    public void saveMeasurement() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
        Measurement saved = measurementRepo.findFirstByOrderByOidDesc();
        assertEquals(Long.valueOf(measurement.time), saved.getTime());
    }

    @Test(expected = NoObjectFoundException.class)
    public void saveMeasurementNoBuilding() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = rndI(-10, -1);
            time = now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
    }

    @Test(expected = NoObjectFoundException.class)
    public void saveMeasurementNoSensor() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = rndI(-10, -1);
            ;
            time = now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
    }

    @Test(expected = NoObjectFoundException.class)
    public void saveMeasurementSensorBelongsToAnotherBuilding() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 3;
            time = now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
    }

    @Test(expected = NoObjectFoundException.class)
    public void saveMeasurementEmpty() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel();
        generalService.saveMeasurement(measurement);
    }

    @Test(expected = NullPointerException.class)
    public void saveMeasurementNull() throws Exception {
        generalService.saveMeasurement(null);
    }

    @Test(expected = BadDataException.class)
    public void saveMeasurementNegativeTime() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = -now();
            value = rndD();
        }};
        generalService.saveMeasurement(measurement);
    }

    @Test
    public void saveMeasurementThroughRequest() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        String body = (new ObjectMapper()).valueToTree(measurement).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false));

        generalService.saveMeasurement(measurement);
        Measurement saved = measurementRepo.findFirstByOrderByOidDesc();
        assertEquals(Long.valueOf(measurement.time), saved.getTime());
    }

    @Test
    public void saveMeasurementThroughRequestWithDoubles() throws Exception {
        SaveMeasurementBadDataMock badData = new SaveMeasurementBadDataMock() {{
            sensorOid = 1d;
            buildingOid = 1d;
            time = 2d;
            value = 100500d;
        }};
        String body = (new ObjectMapper()).valueToTree(badData).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false));
    }

    @Test
    public void saveMeasurementThroughRequestWithLongs() throws Exception {
        SaveMeasurementBadDataMock badData = new SaveMeasurementBadDataMock() {{
            sensorOid = 1L;
            buildingOid = 1L;
            time = 2L;
            value = 100500L;
        }};
        String body = (new ObjectMapper()).valueToTree(badData).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false));
    }

    @Test
    public void saveMeasurementThroughRequestBadDataWithString() throws Exception {
        SaveMeasurementBadDataMock badData = new SaveMeasurementBadDataMock() {{
            sensorOid = "bbb";
            buildingOid = "aaa";
            time = "123s1230";
            value = "aaaaddd*/azxc23123";
        }};
        String body = (new ObjectMapper()).valueToTree(badData).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void saveMeasurementThroughRequestBadDataWithBooleans() throws Exception {
        SaveMeasurementBadDataMock badData = new SaveMeasurementBadDataMock() {{
            sensorOid = true;
            buildingOid = false;
            time = true;
            value = false;
        }};
        String body = (new ObjectMapper()).valueToTree(badData).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void saveMeasurementThroughRequestBadDataMixed() throws Exception {
        SaveMeasurementBadDataMock badData = new SaveMeasurementBadDataMock() {{
            sensorOid = 1;
            buildingOid = 1;
            time = false;
            value = "true";
        }};
        String body = (new ObjectMapper()).valueToTree(badData).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void saveMeasurementsThroughRequestAsync() throws Exception {
        for (int i = 0; i < 10; i++) {
            int index = i;
            new Thread(() -> new SaveMeasurementTestRunnable(index).run()).start();
        }
    }

    @Test
    public void latestMeasurementExistsAfterSave() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        String body = (new ObjectMapper()).valueToTree(measurement).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print());
        Measurement latestMeasurement = sensorRepo.findByOid(1).getLatestMeasurement();
        assertNotNull(latestMeasurement);
    }

    @Test
    public void latestMeasurementUpdated() throws Exception {
        SaveMeasurementModel old = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        String OldBody = (new ObjectMapper()).valueToTree(old).toString();
        this.mockMvc.perform(post("/api/save").content(OldBody).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print());
        Thread.sleep(SECOND);

        SaveMeasurementModel latest = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        String latestBody = (new ObjectMapper()).valueToTree(latest).toString();
        this.mockMvc.perform(post("/api/save").content(latestBody).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print());

        Measurement latestFromDb = sensorRepo.findByOid(1).getLatestMeasurement();
        Integer realLatestOid = measurementRepo.findBySensorOid(1).stream().max(Comparator.comparingLong(Measurement::getTime)).get().getOid();
        assertEquals(realLatestOid, latestFromDb.getOid());
        assertEquals(Long.valueOf(latest.time), latestFromDb.getTime());
        assertEquals(latest.value, latestFromDb.getValue(), 0.0);
    }

    @Test
    public void latestMeasurementNotUpdated() throws Exception {
        SaveMeasurementModel oldButLatestByTime = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        String OldBody = (new ObjectMapper()).valueToTree(oldButLatestByTime).toString();
        this.mockMvc.perform(post("/api/save").content(OldBody).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print());

        Thread.sleep(SECOND);
        SaveMeasurementModel newButNotLatest = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now() - DAY;
            value = rndD();
        }};
        String body = (new ObjectMapper()).valueToTree(newButNotLatest).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print());

        Measurement latestByOid = measurementRepo.findBySensorOid(1).stream().max(Comparator.comparingLong(Measurement::getOid)).get();
        Measurement latestByTime = sensorRepo.findByOid(1).getLatestMeasurement();
        assertNotEquals(latestByOid.getOid(), latestByTime.getOid());
    }


    class SaveMeasurementTestRunnable implements Runnable {

        private final int index;

        SaveMeasurementTestRunnable(int index) {
            this.index = index;
        }

        @Override
        public void run() {
            String error = null;
            final long rndMillis = rndL();
            SaveMeasurementModel measurement = new SaveMeasurementModel() {{
                sensorOid = 1;
                buildingOid = 4;
                time = now() + rndMillis * index;
                value = rndD();
            }};
            String body = (new ObjectMapper()).valueToTree(measurement).toString();

            try {
                Thread.sleep(rndMillis + index);
                mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.error").value(false));
            } catch (Exception e) {
                error = e.getMessage();
            }

            try {
                generalService.saveMeasurement(measurement);
            } catch (Exception e) {
                error = e.getMessage();
            }

            Measurement saved = measurementRepo.findFirstByOrderByOidDesc();
            assertEquals(Long.valueOf(measurement.time), saved.getTime());
            assertNull(error);
        }
    }
}

