package APP.operations;

import APP.BLL.models.LatestValuesResponseModel;
import APP.BLL.models.SaveMeasurementModel;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.entities.Building;
import APP.DAL.entities.Measurement;
import APP.DAL.entities.Sensor;
import APP.DAL.repositories.BuildingRepo;
import APP.DAL.repositories.MeasurementRepo;
import APP.DAL.repositories.SensorRepo;
import APP.UTIL.exceptions.NoObjectFoundException;
import APP.general.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static APP.UTIL.commons.Constants.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Ilya
 * Created on 09.08.2019
 **/

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest()
public class GetLatestByBuildingTest extends BaseTest {

    @Autowired
    private GeneralServiceImpl generalService;

    @Autowired
    private BuildingRepo buildingRepo;

    @Autowired
    private SensorRepo sensorRepo;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getLatestByBuilding() throws Exception {
        SaveMeasurementModel sensor_1_measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Thread.sleep(SECOND / 10);

        SaveMeasurementModel sensor_2_measurement = new SaveMeasurementModel() {{
            sensorOid = 2;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Thread.sleep(SECOND / 10);

        SaveMeasurementModel sensor_3_measurement = new SaveMeasurementModel() {{
            sensorOid = 3;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};

        generalService.saveMeasurement(sensor_1_measurement);
        generalService.saveMeasurement(sensor_2_measurement);
        generalService.saveMeasurement(sensor_3_measurement);

        HashMap<Integer, Object> values = generalService.getLatestValuesByBuilding(1).sensorOidToValue;
        assertEquals((Double) values.get(1), sensor_1_measurement.value, 0.0);
        assertEquals((Double) values.get(2), sensor_2_measurement.value, 0.0);
        assertEquals((Double) values.get(3), sensor_3_measurement.value, 0.0);
    }

    @Test(expected = NoObjectFoundException.class)
    public void getLatestByBuildingNoBuilding() throws NoObjectFoundException {
        generalService.getLatestValuesByBuilding(-1);
    }

    @Test
    public void getLatestByBuildingNoSensors() throws NoObjectFoundException {
        Building newBuilding = buildingRepo.save(new Building());
        HashMap<Integer, Object> sensorOidToValue = generalService.getLatestValuesByBuilding(newBuilding.getOid()).sensorOidToValue;
        assertEquals(0, sensorOidToValue.size());
    }

    @Test
    public void getLatestByBuildingOneOfSensorsHasNoLatestButHasPreviousValues() throws Exception {
        Building newBuilding = buildingRepo.save(new Building());

        // standard save process
        Sensor sensor = new Sensor(newBuilding);
        sensorRepo.save(sensor);
        SaveMeasurementModel model = getModel(newBuilding.getOid(), sensor.getOid(), now(), 100d);
        generalService.saveMeasurement(model);

        // skip latest measurement update
        Sensor sensorWithoutLatestValue = new Sensor(newBuilding);
        sensorRepo.save(sensorWithoutLatestValue);
        Measurement measurement_1 = new Measurement(sensorWithoutLatestValue, now(), 1d);
        Measurement measurement_2 = new Measurement(sensorWithoutLatestValue, now() + SECOND * 2, 2d);
        Measurement measurement_3 = new Measurement(sensorWithoutLatestValue, now() + SECOND, 3d);
        measurementRepo.save(measurement_1);
        measurementRepo.save(measurement_2);
        measurementRepo.save(measurement_3);

        LatestValuesResponseModel latestValuesByBuilding = generalService.getLatestValuesByBuilding(newBuilding.getOid());
        assertEquals(100d, latestValuesByBuilding.sensorOidToValue.get(sensor.getOid()));
        assertEquals(2d, latestValuesByBuilding.sensorOidToValue.get(sensorWithoutLatestValue.getOid()));
    }

    @Test
    public void getLatestByBuildingNoMeasurementsReturnsNoData() throws NoObjectFoundException {
        Building newBuilding = buildingRepo.save(new Building());
        Sensor newSensor_1 = new Sensor();
        newSensor_1.setBuilding(newBuilding);
        Sensor newSensor_2 = new Sensor();
        newSensor_2.setBuilding(newBuilding);
        Sensor newSensor_3 = new Sensor();
        newSensor_3.setBuilding(newBuilding);
        sensorRepo.save(newSensor_1);
        sensorRepo.save(newSensor_2);
        sensorRepo.save(newSensor_3);

        HashMap<Integer, Object> sensorOidToValue = generalService.getLatestValuesByBuilding(newBuilding.getOid()).sensorOidToValue;
        assertEquals(3, sensorOidToValue.size());
        sensorOidToValue.forEach((key, value) -> {
            assertEquals(value, NO_MEASUREMENTS);
        });
    }

    @Test
    public void getLatestValuesByBuildingThroughRequest() throws Exception {
        SaveMeasurementModel sensor_1_measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Thread.sleep(SECOND / 10);

        SaveMeasurementModel sensor_2_measurement = new SaveMeasurementModel() {{
            sensorOid = 2;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};
        Thread.sleep(SECOND / 10);

        SaveMeasurementModel sensor_3_measurement = new SaveMeasurementModel() {{
            sensorOid = 3;
            buildingOid = 1;
            time = now();
            value = rndD();
        }};

        generalService.saveMeasurement(sensor_1_measurement);
        generalService.saveMeasurement(sensor_2_measurement);
        generalService.saveMeasurement(sensor_3_measurement);

        MvcResult result = this.mockMvc.perform(get("/api/building-current").param("oid", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andReturn();

        String resultAsString = result.getResponse().getContentAsString();
        assertTrue(resultAsString.contains(String.valueOf(sensor_1_measurement.value)));
        assertTrue(resultAsString.contains(String.valueOf(sensor_2_measurement.value)));
        assertTrue(resultAsString.contains(String.valueOf(sensor_3_measurement.value)));
    }

    @Test
    public void getLatestValuesByBuildingThroughRequestNoError() throws Exception {
        this.mockMvc.perform(get("/api/building-current").param("oid", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false));
    }

    @Test
    public void saveAndThenGetLatestValuesByBuildingThroughRequest() throws Exception {
        SaveMeasurementModel measurement = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = 100d;
        }};
        String body = (new ObjectMapper()).valueToTree(measurement).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE));

        this.mockMvc.perform(get("/api/building-current").param("oid", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.v.1").value(100));
    }

    @Test
    public void saveNotLatestByTimeAndThenGetRealLatestThroughRequest() throws Exception {
        SaveMeasurementModel latestByTime = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now();
            value = 100d;
        }};
        String body = (new ObjectMapper()).valueToTree(latestByTime).toString();
        this.mockMvc.perform(post("/api/save").content(body).contentType(MediaType.APPLICATION_JSON_VALUE));

        SaveMeasurementModel latestByOid = new SaveMeasurementModel() {{
            sensorOid = 1;
            buildingOid = 1;
            time = now() - MINUTE;
            value = -2d;
        }};
        String latestByOidBody = (new ObjectMapper()).valueToTree(latestByOid).toString();
        this.mockMvc.perform(post("/api/save").content(latestByOidBody).contentType(MediaType.APPLICATION_JSON_VALUE));

        this.mockMvc.perform(get("/api/building-current").param("oid", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.v.1").value(100));
    }

    @Test
    @Transactional
    public void getLatestValuesByBuildingThroughRequestHasOnlyOwnSensors() throws Exception {
        Building building = buildingRepo.findByOid(1);
        List<Integer> sensorsOids = building.getSensors().stream().map(Sensor::getOid).collect(Collectors.toList());
        MvcResult result = this.mockMvc.perform(get("/api/building-current").param("oid", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        LatestValuesResponseModel response = mapper.readValue(result.getResponse().getContentAsString(), LatestValuesResponseModel.class);
        assertEquals(sensorsOids.size(), response.sensorOidToValue.size());
        Set<Integer> resultOids = response.sensorOidToValue.keySet();
        sensorsOids.forEach(oid -> assertTrue(resultOids.contains(oid)));
    }
}
