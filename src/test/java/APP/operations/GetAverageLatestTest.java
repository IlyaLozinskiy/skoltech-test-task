package APP.operations;

import APP.BLL.models.AverageValuesResponseModel;
import APP.BLL.models.LatestValuesResponseModel;
import APP.BLL.models.SaveMeasurementModel;
import APP.BLL.services.GeneralServiceImpl;
import APP.DAL.entities.Building;
import APP.DAL.entities.Sensor;
import APP.DAL.repositories.BuildingRepo;
import APP.DAL.repositories.MeasurementRepo;
import APP.DAL.repositories.SensorRepo;
import APP.general.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.sun.tools.javac.util.List;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashMap;

import static APP.UTIL.commons.Constants.SECOND;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Ilya
 * Created on 09.08.2019
 **/

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest()
public class GetAverageLatestTest extends BaseTest {

    @Autowired
    private GeneralServiceImpl generalService;

    @Autowired
    private BuildingRepo buildingRepo;

    @Autowired
    private SensorRepo sensorRepo;

    @Autowired
    private MeasurementRepo measurementRepo;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAverageValuesHasResult() {
        AverageValuesResponseModel averageValues = generalService.getAverageValues();
        assertNotNull(averageValues.buildingOidToValue);
        assertTrue(averageValues.buildingOidToValue.size() > 0);
    }

    @Test
    public void getAverageValuesResultIsExpected() throws Exception {
        Building newBuilding_1 = buildingRepo.save(new Building());
        Building newBuilding_2 = buildingRepo.save(new Building());
        Sensor newSensor_1 = new Sensor();
        newSensor_1.setBuilding(newBuilding_1);
        Sensor newSensor_2 = new Sensor();
        newSensor_2.setBuilding(newBuilding_1);
        Sensor newSensor_3 = new Sensor();
        newSensor_3.setBuilding(newBuilding_2);
        Sensor newSensor_4 = new Sensor();
        newSensor_4.setBuilding(newBuilding_2);
        sensorRepo.save(newSensor_1);
        sensorRepo.save(newSensor_2);
        sensorRepo.save(newSensor_3);
        sensorRepo.save(newSensor_4);
        SaveMeasurementModel new_measurement_1 = getModel(newBuilding_1.getOid(), newSensor_1.getOid(), now() + SECOND, 1d);
        SaveMeasurementModel new_measurement_2 = getModel(newBuilding_1.getOid(), newSensor_2.getOid(), now(), 2d);
        SaveMeasurementModel new_measurement_3 = getModel(newBuilding_2.getOid(), newSensor_3.getOid(), now() - SECOND, 3d);
        SaveMeasurementModel new_measurement_4 = getModel(newBuilding_2.getOid(), newSensor_4.getOid(), now(), 4d);
        generalService.saveMeasurement(new_measurement_1);
        generalService.saveMeasurement(new_measurement_2);
        generalService.saveMeasurement(new_measurement_3);
        generalService.saveMeasurement(new_measurement_4);

        HashMap<Integer, Object> oidToValue = generalService.getAverageValues().buildingOidToValue;
        Object expected_1 = oidToValue.get(newBuilding_1.getOid());
        Object expected_2 = oidToValue.get(newBuilding_2.getOid());
        logger.info(String.valueOf(expected_1));
        logger.info(String.valueOf(expected_2));
        assertEquals(1.5d, (Double) expected_1, 0.0);
        assertEquals(3.5d, (Double) expected_2, 0.0);
    }

    @Test
    public void getAverageValueResultCount() {
        int expectedSize = Lists.newArrayList(buildingRepo.findAll()).size();
        AverageValuesResponseModel averageValues = generalService.getAverageValues();
        assertEquals(expectedSize, averageValues.buildingOidToValue.size());
    }

    @Test
    public void getLatestValuesByBuildingThroughRequest() throws Exception {
        this.mockMvc.perform(get("/api/average-current")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.v").isMap())
                .andExpect(jsonPath("$.v").isNotEmpty());
    }

    @Test
    public void getLatestValuesByBuildingThroughRequestCountRight() throws Exception {
        int expectedSize = Lists.newArrayList(buildingRepo.findAll()).size();
        MvcResult result = this.mockMvc.perform(get("/api/average-current")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        LatestValuesResponseModel response = mapper.readValue(result.getResponse().getContentAsString(), LatestValuesResponseModel.class);
        assertEquals(expectedSize, response.sensorOidToValue.size());
    }

    @Test
    public void getAverageValuesResultIsExpectedThroughRequest() throws Exception {
        Building newBuilding = buildingRepo.save(new Building());
        Sensor newSensor_1 = new Sensor();
        newSensor_1.setBuilding(newBuilding);
        Sensor newSensor_2 = new Sensor();
        newSensor_2.setBuilding(newBuilding);
        Sensor newSensor_3 = new Sensor();
        newSensor_3.setBuilding(newBuilding);
        Sensor newSensor_4 = new Sensor();
        newSensor_4.setBuilding(newBuilding);
        sensorRepo.save(newSensor_1);
        sensorRepo.save(newSensor_2);
        sensorRepo.save(newSensor_3);
        sensorRepo.save(newSensor_4);
        SaveMeasurementModel new_measurement_1 = getModel(newBuilding.getOid(), newSensor_1.getOid(), now() + SECOND, 1d);
        SaveMeasurementModel new_measurement_2 = getModel(newBuilding.getOid(), newSensor_2.getOid(), now(), 2d);
        SaveMeasurementModel new_measurement_3 = getModel(newBuilding.getOid(), newSensor_3.getOid(), now() - SECOND, 3d);
        SaveMeasurementModel new_measurement_4 = getModel(newBuilding.getOid(), newSensor_4.getOid(), now(), 4d);
        generalService.saveMeasurement(new_measurement_1);
        generalService.saveMeasurement(new_measurement_2);
        generalService.saveMeasurement(new_measurement_3);
        generalService.saveMeasurement(new_measurement_4);

        this.mockMvc.perform(get("/api/average-current")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.error").value(false))
                .andExpect(jsonPath("$.v." + newBuilding.getOid().toString()).value(2.5d));

    }
}
