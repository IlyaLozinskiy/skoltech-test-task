# Test project for Skoltech


### Functionality

In accordance with the technical task, this application is able to perform following operations

1. Save measurement data from the object (building)
2. Output data from specific sensor for a certain period of time (it is also possible to fetch all the history if no period is specified)
3. Return the latest measurements per sensor for the given building
4. Return the average values from latest measurements for all buildings

In addition, the app also does:

1. Collects logs on daily basis 
2. Returns server status
     
### API

Most request and response parameter names are minified as abbreviations of the first letter of the corresponding property.
In responses from the server, arrays were replaced with maps between the object id (building, sensor or measurement)
and the requested property. This was done to reduce the amount of information transmitted between the client and server.
For full names definitions see: `APP.BLL.models`

| Path     | Params    | Description |
| --------|---------|-------|
| /api/status  | -   | server status |
|  |  |  |
| /api/save | for params see: `SaveMeasurementModel.java` | save measurement |
|  |  |  |
| /api/sensor | ``int oid`` - sensor id (required) | measurements for specific sensor by time period |
|             | ``long start`` - bottom edge of time frame | |
|             | ``long end`` - top edge of time frame | |
|  |  |  |
| /api/building-current | ``int oid`` - building id (required) | latest value by building |
|  |  |  |
| /api/average-current | - | average of the latest values for all buildings |


### Testing includes
 * Unit tests that check key functions separately from other parts of the application.
 * Integration tests involving several or all levels of the application
 * State testing
 
 
### Technologies used

* Java 8 (jdk 11.0.4)
* Spring Boot 2.1.6
* JUnit 4.12
* MySql 8.0.17
* Gradle 5.5.1
* OS: Windows 8.1
* IntelliJ Idea 2019.2


#### _P.S._
_Database initialization happens automatically due to hibernate. To run on your machine,_
_please change datasource connection config in `application.properties` file. `import.sql` includes_
_the first part of initial dataset for testing purposes. For the second (much bigger) part see: `Application.java`_

_Most likely you will need to reconfigure the project (jdk, gradle, paths, etc.) since I am using Windows 8.1._
_Feel free to ask any questions you may have, I will be happy to help!_
